global with sharing class SpeechRecognizationController_ver2 {

@RemoteAction
global static string checkAvailability(string passedValue)
{
 
   if(passedValue!=null)
   {
      List<string> splitVa = passedValue.split(' ');
      if(splitVa[0].toLowerCase().contains('account')) 
      {
         string s = 'select id from Account where name like \'%'+splitVa[1]+'%\' limit 1';
         system.debug(s);
          List<Account> accountList = Database.query(s);
          if(accountList!=null && accountList.size()>0)
             return accountList[0].id;
      }
   }
  return '';
}

//redirect to page
@RemoteAction
global static PageReference redirect(string passedValue)
{
  PageReference pg = null;
 pg =  new PageReference('/passedValue');
 pg.setRedirect(true);
  return pg;
}

@RemoteAction
global static string createCase(string sub,string des,string accId)
{
   Case case1 = new Case(subject=sub,Description=des,AccountId=accId);
   insert case1;
   return 'success';
}

//for adding notes to account
@RemoteAction
global static string createNote(string tit,string bodyDes,string accId)
{
   Note addNote = new Note(Title=tit,Body=bodyDes,ParentId=accId);
   insert addNote;
   return 'success';
}
/*
//for chatter post
@RemoteAction
global static string chatterPost(string entyId)
{
   //ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), entyId, ConnectApi.FeedElementType.FeedItem, 'posted successfully!');
    FeedItem post = new FeedItem();
    post.ParentId = entyId;
    post.Body = 'new chatter feed item';
    insert post;

    return 'success';
}*/
}