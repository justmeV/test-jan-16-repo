public class XML_Controller {
public Blob contentfile {get;set;}
public String filename {get;set;}
public string selectedvalue{get;set;}
public string attach {get;set;}
public List<SelectOption> selectValues { get; set; }
    
    public XML_Controller()
    {
        selectValues = new List<SelectOption>();
        populateControllingPicklistValues();        
    }
    public void populateControllingPicklistValues(){
        selectValues.add(new SelectOption('--Select Object--','--Select Object--'));
        selectValues.add(new SelectOption('Account','Account'));
        selectValues.add(new SelectOption('Project__c','Project'));                       
    }
    public pageReference SubmitAction()
    { 
     /*
        //One way to find the list of custom objects in org..
        List<String> CustomObjectsList  = new List<String>();
        List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values();  
        for(Schema.SObjectType f : gd)
        {
            if (f.getDescribe().isCustom()){
                CustomObjectsList.add(f.getDescribe().getName());
            }
        }
        system.debug('test custom!!!!!!!!!'+CustomObjectsList);  */
        
        if(selectedvalue != null && selectedvalue != '--Select Object--')
        {    
            if(filename != null)
            {           
                Integer indexvalue=filename.lastindexof('.');
                if((filename.substring((indexvalue+1),(indexvalue+4)).equalsignorecase('xml')))
                {
                    attach=contentfile.toString();
                    system.debug('attach!!!!!!!!'+attach);
                    system.debug('selected value!!!!!!!!'+selectedvalue);
                    List<First_Setting__c> customsetting=First_Setting__c.getAll().Values();
                    Map<String,Sobject> customsettingmap=new Map<String,Sobject>();
                    Map<String,String> customsettingMap1=new Map<String,String>();
                    String objectName='';
                    if(selectedvalue.contains('__c'))
                    {
                        objectName =  selectedvalue.replace('__c','');
                    }
                    else
                    {
                        objectName =  selectedvalue;
                    }
                    system.debug('so objectName is!!!!!!!!'+objectName);
                    for(First_Setting__c obj:customsetting)
                    {
                        if(objectName != null)
                        {
                            if(obj.Object__c.contains(objectName))
                            {
                                customsettingmap.put(obj.Name,obj);
                            }
                        }
                    }    
                    // Using XML Parser to set values to fields of object..           
                    XML_GeneralisingXMLParser parser=new XML_GeneralisingXMLParser();
                    List<Sobject> listToBeInserted=parser.parse(attach,selectedvalue,customsettingmap);
                   
                    if(listToBeInserted.size()>0)
                    {
                        for(Sobject abc:listToBeInserted)
                        {
                            String nameofrecordtype=(String)abc.get('recordtypeId');
                            system.debug('name of rt!!!!!!!!'+nameofrecordtype);
                            try
                            {
                                if(nameofrecordtype != null)
                                {
                                    abc.put('recordtypeId',([select id from Recordtype where name=: nameofrecordtype].Id));
                                }
                            }
                            catch(Exception e)
                            {
                                if(e.getMessage().contains('List has no rows for assignment to SObject'))
                                {
                                    Sobject obj;
                                    Schema.SObjectType targetType = Schema.getGlobalDescribe().get(selectedvalue);
                                    obj = targetType.newSObject();
                                    
                                    //Schema.DescribeSObjectResult describeAcc = Schema.SObjectType.obj;
                                    Schema.DescribeSObjectResult describeAcc = targetType.getDescribe();
                                    List<Schema.RecordTypeInfo> rtInfos = describeAcc.getRecordTypeInfos();
                                    for(Schema.RecordTypeInfo rtInfo : rtInfos) {
                                        if(rtInfo.isDefaultRecordTypeMapping()) {               
                                            abc.put('recordtypeId',rtInfo.getRecordTypeId());                                    
                                            break;
                                        }
                                    }
                                    system.debug('abc value from catch!!!!!!!1'+abc);
                                }
                            }
                        }
                    }
                    system.debug('Account to be inserted!!!!!!!!!'+listToBeInserted);
                    try
                    {
                        insert listToBeInserted;
                        ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.INFO,'Successfully Inserted');     
                        ApexPages.addMessage(mess);
                    }
                    catch(Exception e)
                    {
                        ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.ERROR,e.getMessage());
                        ApexPages.addMessage(mess);
                    } 
                }
                else
                {
                    ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.ERROR,'The file you are trying to upload is not a \' .XML \' type.');
                    ApexPages.addMessage(mess);
                }
            }
            else
            {
                ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.ERROR,'select a file to upload!');
                ApexPages.addMessage(mess);
            }         
        }
        else
        {
            ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.ERROR,'select a object!');
            ApexPages.addMessage(mess);
        }    
        return null;
    }
}