/* Just Testing for Jenkins push/deploy.. Dont worry! */

public class TestController { 
public PageReference displayException() { 
Account a = (Account) Account.SObjectType.newSObject(null, true); 
a.addError('This is a custom error'); //Error occurs - SObject row does not allow errors ..
//ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, '*FAILED*')); // It'll work..
return null; 
} 

public PageReference displayError() { 
Account a = (Account) Account.SObjectType.newSObject(null, false); 
a.addError('This is a custom error'); 
return null; 
} 

public Project__c pjt{get;set;}
public TestController ()
{
    pjt = [select id,name,createdDate from Project__c where id=: 'a029000000OoBFj'];
    //system.debug(pjt.createdDate.format());
    //DateTime dtValue = (DateTime)pjt.get('createdDate');
    DateTime dtValue = DateTime.Now(); 
    system.debug('dtValue!!!!!!!'+dtValue);
    String dtFormatted = dtValue.format('MMM dd, yyyy');    
    system.debug('formatted date is!!!!!!!'+dtFormatted);
    system.debug('month!!!'+dtFormatted.substring(0,3) +'year!!!!!!'+ dtFormatted.substring(8,12));
}

}